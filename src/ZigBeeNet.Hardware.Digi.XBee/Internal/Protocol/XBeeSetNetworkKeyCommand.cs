//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZigBeeNet.Hardware.Digi.XBee.Internal.Protocol
{
    
    
    /// <summary>
    /// Class to implement the XBee command " Set Network Key ".
    /// AT Command <b>NK</b></p>Set the 128-bit AES network encryption key. This command is
    /// write-only and cannot be read. If set to 0 (default), the device selects a random network key. 
    ///This class provides methods for processing XBee API commands.
    ///
    /// </summary>
    public class XBeeSetNetworkKeyCommand : XBeeFrame, IXBeeCommand 
    {
    }
}
